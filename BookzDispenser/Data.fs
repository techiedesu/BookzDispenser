module BookzDispenser.Data

open System.ComponentModel.DataAnnotations
open Microsoft.EntityFrameworkCore

[<CLIMutable>]
type Book =
    { [<Required>] Id: string
      [<Required>] FileName: string
      [<Required>] Extension: string
      [<Required>] Data: byte[] }

type AppDbContext(options: DbContextOptions<AppDbContext>) =
    inherit DbContext(options)

    [<DefaultValue>]
    val mutable _books : DbSet<Book>
    member public this.Books with get() = this._books and set value = this._books <- value
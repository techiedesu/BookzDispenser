module BookzDispenser.App

open System.IO
open BookzDispenser.Data
open Microsoft.AspNetCore.Builder
open Microsoft.EntityFrameworkCore
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Microsoft.Extensions.Logging

let webApp =
    choose [
        POST   >=> route "/book" >=> BookHandlers.createBook
        GET    >=> routef "/book/%s" BookHandlers.getBook
        DELETE >=> routef "/book/%s" BookHandlers.deleteBook
]

type Startup() =
    member this.ConfigureServices (services : IServiceCollection) =
        let cs = "Server=localhost;Database=bookz_dispenser;Uid=root;Pwd=root;"
        services.AddDbContext<AppDbContext>(fun options -> options.UseMySql(cs, ServerVersion.AutoDetect(cs)) |> ignore) |> ignore
        services.AddGiraffe() |> ignore

    member this.Configure (app : IApplicationBuilder) =
        app.UseGiraffe webApp

module DatabaseMigrator =
    let inline migrate(scope: IServiceScope) =
        use loggerFactory = LoggerFactory.Create(fun b -> b.AddConsole() |> ignore)
        let logger = loggerFactory.CreateLogger("DatabaseMigrator")

        let ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>()
        let pendingMigrations = ctx.Database.GetPendingMigrations()
        let pendingMigrations = pendingMigrations |> Seq.toArray

        if pendingMigrations.Length > 0 then
            logger.LogInformation("DbContext outdated. Migrating...");
            pendingMigrations |> Array.iter(fun m -> logger.LogInformation("Migration: {Migration}", m))
            ctx.Database.Migrate()
            logger.LogInformation("Migrations has been applied!")
        else
            logger.LogInformation("DbContext updated. Migrations not needed.")

    let seed(scope: IServiceScope) =
        let ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>()

        if ctx.Books.CountAsync().ConfigureAwait(false).GetAwaiter().GetResult() = 0 then
            let createBookEntity fileName : Book =
                let bytes = File.ReadAllBytes fileName
                let fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName)
                { Id = fileNameWithoutExtension
                  FileName = fileNameWithoutExtension
                  Extension = Path.GetExtension(fileName)
                  Data = bytes }

            Directory.GetFiles("SeedBooks") |> Array.map createBookEntity |> Array.iter(fun b -> ctx.Add b |> ignore)
            ctx.SaveChanges() |> ignore
namespace BookzDispenser

open System
open BookzDispenser.Data

module Dtos =
    type CreateBookRequest =
        { FileName: string
          Extension: string
          BookData: byte[] }

open Dtos

[<RequireQualifiedAccess>]
module BookHandlers =

    open System.Threading.Tasks
    open Microsoft.AspNetCore.Http
    open Microsoft.EntityFrameworkCore
    open Microsoft.Extensions.DependencyInjection
    open Giraffe

    /// Add book and return Id
    let createBook =
        fun (next : HttpFunc) (ctx : HttpContext) ->
            try
                let dbContext = ctx.RequestServices.GetService<AppDbContext>()
                let createRequest = ctx.BindJsonAsync<CreateBookRequest>().ConfigureAwait(false).GetAwaiter().GetResult()

                let book : Book =
                    { Id = Guid.NewGuid().ToString()
                      FileName = createRequest.FileName
                      Extension = createRequest.Extension
                      Data = createRequest.BookData }

                dbContext.Add(book) |> ignore
                dbContext.SaveChanges() |> ignore

                json({| Id = book.Id |}) next ctx
            with e ->
                json({| Error = e.Message; StackTrace = e.StackTrace |}) next ctx

    /// Get book by Id
    let getBook bookId =
        fun (_ : HttpFunc) (ctx : HttpContext) ->
            try
                let dbContext = ctx.RequestServices.GetService<AppDbContext>()
                let book = dbContext.Books.SingleAsync(fun b -> b.Id = bookId)
                               .ConfigureAwait(false)
                               .GetAwaiter()
                               .GetResult()

                dbContext.Add(book) |> ignore

                ctx.WriteJsonAsync book
            with e ->
                ctx.SetStatusCode(400)
                ctx.WriteJsonAsync {| Error = e.Message; StackTrace = e.StackTrace |}

    /// Remove book by Id
    let deleteBook bookId =
        fun (_ : HttpFunc) (ctx : HttpContext) ->
            try
                let dbContext = ctx.RequestServices.GetService<AppDbContext>()
                let book = dbContext.Books.SingleAsync(fun b -> b.Id = bookId)
                               .ConfigureAwait(false)
                               .GetAwaiter()
                               .GetResult()

                dbContext.Books.Remove(book) |> ignore
                dbContext.SaveChanges() |> ignore
                ctx.SetStatusCode 204
                ctx.NotModifiedResponse() |> Task.FromResult
            with e ->
                ctx.SetStatusCode(400)
                ctx.WriteJsonAsync({| Error = e.Message; StackTrace = e.StackTrace |})
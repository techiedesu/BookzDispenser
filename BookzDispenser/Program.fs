module BookzDispenser.Entry

open BookzDispenser.App
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.DependencyInjection

let CreateHostBuilder args =
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(fun webBuilder ->
            webBuilder.UseStartup<Startup>() |> ignore
        )

// MySQL for faggots.
[<EntryPoint>]
let main args =
    let host = CreateHostBuilder(args).Build()

    use scope = host.Services.CreateScope()
    DatabaseMigrator.migrate scope
    DatabaseMigrator.seed scope

    host.Run()
    0